using Microsoft.ApplicationInsights.Extensibility;
using OIct.PP.PdfGenerator.Application.Interfaces;
using OIct.PP.PdfGenerator.Application.Services;
using OIct.PP.PdfGenerator.Application.Settings;
using OIct.PP.PdfGenerator.Middleware;
using OIct.PP.PdfGenerator.Telemetry;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddApplicationInsightsTelemetry();
builder.Services.AddSingleton<ITelemetryInitializer, TelemetryInitializer>();
builder.Services.AddControllers();
builder.Services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IPdfService, PdfService>();
builder.Services.Configure<PdfGeneratorConfiguration>(builder.Configuration);
builder.Services.AddHealthChecks();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();
app.ConfigureCustomExceptionMiddleware();

app.MapHealthChecks("/health");

app.MapControllers();

app.Run();