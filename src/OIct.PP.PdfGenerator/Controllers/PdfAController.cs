using Microsoft.AspNetCore.Mvc;
using OIct.PP.PdfGenerator.Application.Extensions;
using OIct.PP.PdfGenerator.Application.Interfaces;
using OIct.PP.PdfGenerator.Application.Model.Request;
using OIct.PP.PdfGenerator.Application.Model.Response;

namespace OIct.PP.PdfGenerator.Controllers;

[Route("[controller]")]
[ApiController]
public class PdfAController : Controller
{
    private readonly IPdfService _pdfService;

    public PdfAController(IPdfService pdfService)
    {
        _pdfService = pdfService ?? throw new ArgumentNullException(nameof(pdfService));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <remarks></remarks>
    [HttpPost("generate")]
    [ProducesResponseType(typeof(FileResult),StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [ProducesResponseType(typeof(ErrorsModel),StatusCodes.Status503ServiceUnavailable)]
    public async Task<IActionResult> Generate([FromBody] CreatePdfModel model, CancellationToken cancellationToken)
    {
        var htmlString = model.Base64Html.DecodeFromBase64();
        var creationResponse = await _pdfService.GeneratePdfAAsync(htmlString);

        return File(creationResponse, "application/pdf", "file.pdf");
    }

}