namespace OIct.PP.PdfGenerator.Application.Settings;

public class PdfGeneratorConfiguration
{
    public string IcsProfilePath { get; set; }
    public string FontsDirectoryPath { get; set; }
    public string TargetInfo { get; set; }
    public string PdfAuthor { get; set; }
    public string PdfLanguage { get; set; }
}