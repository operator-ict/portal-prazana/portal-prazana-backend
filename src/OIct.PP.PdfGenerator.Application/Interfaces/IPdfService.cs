using OIct.PP.PdfGenerator.Application.Model.Response;

namespace OIct.PP.PdfGenerator.Application.Interfaces;

public interface IPdfService
{
    Task<byte[]> GeneratePdfAAsync(string html);
}