using System.Text;

namespace OIct.PP.PdfGenerator.Application.Extensions;

public static class StringExtensions
{
    public static string DecodeFromBase64(this string base64String)
    {
        var htmlBytes = Convert.FromBase64String(base64String);
        return Encoding.UTF8.GetString(htmlBytes);
    }
}