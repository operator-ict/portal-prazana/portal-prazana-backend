using iText.Html2pdf;
using iText.Html2pdf.Resolver.Font;
using iText.Kernel.Pdf;
using iText.Pdfa;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OIct.PP.PdfGenerator.Application.Interfaces;
using OIct.PP.PdfGenerator.Application.Settings;

namespace OIct.PP.PdfGenerator.Application.Services;

public class PdfService : IPdfService
{
    private readonly ILogger _logger;
    private readonly PdfGeneratorConfiguration _pdfGeneratorConfiguration;
    private readonly int BufferSize = 128 * 1024;

    public PdfService(ILogger<PdfService> logger, IOptions<PdfGeneratorConfiguration> pdfGeneratorConfiguration)
    {
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _pdfGeneratorConfiguration = pdfGeneratorConfiguration?.Value ?? throw new ArgumentNullException(nameof(pdfGeneratorConfiguration));
    }

    public Task<byte[]> GeneratePdfAAsync(string html)
    {
        var icsProfile = new FileStream(_pdfGeneratorConfiguration.IcsProfilePath, FileMode.Open, FileAccess.Read);
        var memoryStream = new MemoryStream(BufferSize);
        var outputIntent = new PdfOutputIntent("Custom", "", null, _pdfGeneratorConfiguration.TargetInfo, icsProfile);
        var pdfDoc = new PdfADocument(new PdfWriter(memoryStream), PdfAConformanceLevel.PDF_A_3B, outputIntent);
        
        pdfDoc
            .SetTagged()
            .GetCatalog()
            .Put(PdfName.Author, new PdfString(_pdfGeneratorConfiguration.PdfAuthor))
            .SetLang(new PdfString(_pdfGeneratorConfiguration.PdfLanguage));
        
        var converterProperties = new ConverterProperties();
        var fontProvider = new DefaultFontProvider(true, true, true);
        fontProvider.AddDirectory(_pdfGeneratorConfiguration.FontsDirectoryPath);
        converterProperties.SetFontProvider(fontProvider);
        HtmlConverter.ConvertToPdf(html, pdfDoc, converterProperties);

        return Task.FromResult(memoryStream.ToArray());
    }
}