using System.Text.Json.Serialization;

namespace OIct.PP.PdfGenerator.Application.Model.Response;

public class ErrorModel
{
    [JsonPropertyName("kod")]
    public string Code { get; set; }

    [JsonPropertyName("popis")]
    public string Description { get; set; }
}