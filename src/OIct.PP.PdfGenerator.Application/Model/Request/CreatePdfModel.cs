using System.Text.Json.Serialization;

namespace OIct.PP.PdfGenerator.Application.Model.Request;

public class CreatePdfModel
{
    [JsonPropertyName("base64Html")]
    public string Base64Html { get; set; }
}