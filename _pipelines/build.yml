# Yaml stages template for building and pushing containers

parameters:
  # Name of the project. Example: OIct.PP.Catalog
  - name: projectName
    type: string
    default: ProjectWithoutName
  # Connection to Azure docker registry https://dev.azure.com/operatorict/PortalPrazana/_settings/adminservices
  - name: "dockerRegistryConnection"
    type: string
    default: "DockerRegistryPortalPrazana"

stages:
  - stage: Build
    displayName: "Build image"

    variables:
      - group: "PortalPrazanaShared"
      - name: "system.debug"
        value: true
      # Build context directory is named "source" because there we checkout the files from git (see steps.checkout)
      - name: "buildContext"
        value: "$(Agent.BuildDirectory)/source/"
      # Helm directory is named "helm" because there we checkout the files from git (see steps.checkout). Helm repository structure is "PortalPrazana_Helm\helm" therefore /helm/helm
      - name: "helmDirectory"
        value: "$(Agent.BuildDirectory)/helm/helm"
      # sanitize projectName to helmProjectName withs dot symbols
      - name: helmProjectName
        value: $[replace('${{ parameters.projectName }}', '.', '')]
      # enable BUILDKIT
      - name: DOCKER_BUILDKIT
        value: 1

    jobs:
      # Job for develop or master branch
      - job: Build
        displayName: "Build develop or master based on source branch"
        condition: or(eq(variables['Build.SourceBranchName'], 'develop'), eq(variables['Build.SourceBranchName'], 'master'))
        pool:
          vmImage: ubuntu-latest
        workspace:
          clean: all
        steps:
          # We need to set "checkout: self" and "persistCredentials: true" because we do git push and therefore we need oauth token for access git repository
          - checkout: self
            clean: true
            persistCredentials: true
            path: "source"

          # Git checkout for helm repository develop branch
          - checkout: git://PortalPrazana/PortalPrazana_Helm@refs/heads/develop
            clean: true
            condition: eq(variables['Build.SourceBranch'], 'refs/heads/develop')
            persistCredentials: true
            path: "helm"

          # Git checkout for helm repository master branch
          - checkout: git://PortalPrazana/PortalPrazana_Helm@refs/heads/master
            clean: true
            condition: eq(variables['Build.SourceBranch'], 'refs/heads/master')
            persistCredentials: true
            path: "helm"

          # Build ${{ parameters.projectName }}
          - task: Docker@2
            name: "BuildImage"
            displayName: "Build and push image ${{ parameters.projectName }}"
            inputs:
              command: buildAndPush
              containerRegistry: ${{ parameters.dockerRegistryConnection }}
              DockerFile: "$(buildContext)/src/${{ parameters.projectName }}/Dockerfile"
              buildContext: "$(buildContext)/src"
              repository: "${{ parameters.projectName }}/$(Build.SourceBranchName)"
              tags: |
                $(Build.BuildNumber)
                latest

          # Checkout for develop
          - task: CmdLine@2
            name: "GitCheckoutDevelop"
            condition: eq(variables['Build.SourceBranch'], 'refs/heads/develop')
            displayName: "Git checkout develop"
            inputs:
              script: |
                git config user.email build@operatorict.cz
                git config user.name "builder"
                git checkout develop
              workingDirectory: $(helmDirectory)

          # Checkout for master
          - task: CmdLine@2
            name: "GitCheckoutMaster"
            condition: eq(variables['Build.SourceBranch'], 'refs/heads/master')
            displayName: "Git checkout master"
            inputs:
              script: |
                git config user.email build@operatorict.cz
                git config user.name "builder"
                git checkout master
              workingDirectory: $(helmDirectory)

          # Build version in values-<branch>-tag.yaml
          - script: |
              yq eval -i \
              '.$(helmProjectName).buildVersion = "$(Build.BuildNumber)"' \
              $(helmDirectory)/pp-backend/values-$(Build.SourceBranchName)-tag.yaml
            displayName: "Set build version to values-tag.yaml"

          # Git push for changed files in helm repository (like Chart.yaml, values.yaml, etc.)
          - task: CmdLine@2
            name: "GitPush"
            displayName: "Push changes to git"
            inputs:
              script: |
                git commit -a -m "Azure pipelines build Portal Prazana ${{ parameters.projectName }}"
                git pull
                git push
              workingDirectory: $(helmDirectory)
